#version 330

layout(lines) in;
layout(line_strip, max_vertices = 2) out;

in vec4 pos[];
in vec4 color[];

out vec4 gpos;
out vec4 gcolor;

void main() {
    
    // Emit vertices exactly as they arrive
    
    gpos = pos[0];
    gcolor = color[0];
    gl_Position = gl_in[0].gl_Position;
    EmitVertex();

    gpos = pos[1];
    gcolor = color[1];
    gl_Position = gl_in[1].gl_Position;
    EmitVertex();
    
    EndPrimitive();
}
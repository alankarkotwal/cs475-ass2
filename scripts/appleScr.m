apple = rgb2gray(imread('apple.png'));
apple = imresize(apple, 1/4);
appleCenter = [295 254];

sizeApple = size(apple);

for i=1:sizeApple(1)
    for j=1:sizeApple(2)
        if apple(i, j) ~= 255
            apple(i, j) = 0;
        end
    end
end

apple = im2bw(apple);
apple = edge(apple);
cc = bwconncomp(apple);

appleBody = cc.PixelIdxList(1);
appleLeaf = cc.PixelIdxList(2);
appleBody = appleBody{1};
appleLeaf = appleLeaf{1};
%imshow(apple);

appleBodyMat = zeros(3*size(appleBody, 1), 3);

count = 1;
for i=1:size(appleBody)
    
    [z,  y ] = ind2sub(sizeApple, appleBody(i));
    
    if i == size(appleBody, 1)
        [zn, yn] = ind2sub(sizeApple, appleBody(1));
    else
        [zn, yn] = ind2sub(sizeApple, appleBody(i+1));
    end
    
    appleBodyMat(count, 1) = 0;
    appleBodyMat(count, 2) = 0;
    appleBodyMat(count, 3) = 0;
    count = count+1;
    
    appleBodyMat(count, 1) = 0;
    appleBodyMat(count, 2) = y - appleCenter(2);
    appleBodyMat(count, 3) = z - appleCenter(1);
    count = count+1;
    
    appleBodyMat(count, 1) = 0;
    appleBodyMat(count, 2) = yn - appleCenter(2);
    appleBodyMat(count, 3) = zn - appleCenter(1);
    count = count+1;
end

appleLeafMat = zeros(3*size(appleLeaf, 1), 3);

count = 1;
for i=1:size(appleLeaf)
    
    [z,  y ] = ind2sub(sizeApple, appleLeaf(i));
    
    if i == size(appleLeaf, 1)
        [zn, yn] = ind2sub(sizeApple, appleLeaf(1));
    else
        [zn, yn] = ind2sub(sizeApple, appleLeaf(i+1));
    end
    
    appleLeafMat(count, 1) = 0;
    appleLeafMat(count, 2) = 93 - appleCenter(2);
    appleLeafMat(count, 3) = 293 - appleCenter(1);
    count = count+1;
    
    appleLeafMat(count, 1) = 0;
    appleLeafMat(count, 2) = y - appleCenter(2);
    appleLeafMat(count, 3) = z - appleCenter(1);
    count = count+1;
    
    appleLeafMat(count, 1) = 0;
    appleLeafMat(count, 2) = yn - appleCenter(2);
    appleLeafMat(count, 3) = zn - appleCenter(1);
    count = count+1;
end

fullApple = [appleBodyMat; appleLeafMat];
%fullApple = appleBodyMat;

%fullApple = -2.2/80.75 *fullApple/max(max(abs(fullApple)));
fullApple = -fullApple/max(max(abs(fullApple)));
scatter(fullApple(:, 2), fullApple(:, 3));
axis equal;

fullApple = [fullApple ones(size(fullApple, 1), 3)];
csvwrite('apple.csv', fullApple);
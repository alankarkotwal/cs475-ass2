apple = rgb2gray(imread('apple.png'));
apple = imresize(apple, 1/4);
appleCenter = [295 254];

sizeApple = size(apple);

for i=1:sizeApple(1)
    for j=1:sizeApple(2)
        if apple(i, j) ~= 255
            apple(i, j) = 0;
        end
    end
end

apple = im2bw(apple);
apple = edge(apple);
cc = bwconncomp(apple);

appleBody = cc.PixelIdxList(1);
appleLeaf = cc.PixelIdxList(2);
appleBody = appleBody{1};
appleLeaf = appleLeaf{1};

appleBodyMat = zeros(size(appleBody, 1), 2);

for i=1:size(appleBody)
    
    [z,  y ] = ind2sub(sizeApple, appleBody(i));
    
    appleBodyMat(i, 1) = y - appleCenter(2);
    appleBodyMat(i, 2) = z - appleCenter(1);
    
end

scatter(appleBodyMat(:, 1), appleBodyMat(:, 2));

dt = delaunayTriangulation(appleBodyMat);
triplot(dt);
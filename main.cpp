//  Modeller Class for CS475 Assignment-1
//  main.cpp
//  
//
//  Created by Alankar Kotwal on 06/08/15.
//  <alankar.kotwal@iitb.ac.in>
//  12D070010

#include <main.hpp>

int main() {
    
    glfwSetKeyCallback(m.getWindow(), keyCallback);
    glfwSetMouseButtonCallback(m.getWindow(), mouseCallback);
    glfwSetScrollCallback(m.getWindow(), scrollCallback);
    
    m.mainLoop();
    
    return 0;
}
ifeq ($(shell uname), Darwin)
	OPENGLLIB= -framework OpenGL
	GLEWLIB= -lglew
	GLFWLIB= -lglfw3
else
	OPENGLLIB= -lGL
	GLEWLIB= -lGLEW
	GLFWLIB = -lglfw
endif
	
LIBS=$(OPENGLLIB) $(GLEWLIB) $(GLFWLIB)
LDFLAGS=-L/usr/local/lib 
CPPFLAGS=-I/usr/local/include -I. -std=c++11 -O3

BIN=model
SRCS=main.cpp gl_framework.cpp shader_util.cpp model.cpp
INCLUDES=main.hpp gl_framework.hpp shader_util.hpp model.hpp

all: $(BIN)

$(BIN): $(SRCS) $(INCLUDES)
	g++ $(CPPFLAGS) $(SRCS) -o $(BIN) $(LDFLAGS) $(LIBS)

clean:
	rm -f *~ *.o $(BIN)

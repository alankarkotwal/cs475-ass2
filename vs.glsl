#version 330

in vec4 vp;
in vec4 vColor;
out vec4 color;
out vec4 pos;
uniform mat4 uRotationMatrix;

void main () 
{
    gl_Position = uRotationMatrix * vp;
    color = vColor;
    pos = gl_Position;
}
